import React from 'react'
import Paper from 'material-ui/Paper'
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';
import Grid from 'material-ui/Grid';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Checkbox from 'material-ui/Checkbox';
import { FormControlLabel } from 'material-ui/Form';
import validators from '../../validators'
import LoadingScreen from '../common/LoadingScreen'

const styles = theme=> ({
    root: {
        flexGrow: 1,
        // backgroundImage: 'url("https://static.lexpress.fr/medias_9559/w_2000,h_1125,c_fill,g_north/v1399453401/high-tech-ces-petits-francais-qui-cartonnent_4894463.jpg")',

    },
    screen: {
        height: '100vh',
    },
    paper: {
        minWidth: 360,
        minHeight: 290,
        textAlign: 'center',
        opacity: 0.95
    },
    textField: {
        marginTop: theme.spacing.unit,
    },
    buttonSubmit: {
        margin: theme.spacing.unit * 2,
        textTransform: 'none'
    },
    buttonForgot: {
        textTransform: 'none'
    },
    formWrapper: {
        margin: 'auto'
    },
    checkBoxWrapper: {
        marginTop: theme.spacing.unit,
    },
    checkBox: {
        margin: 0
    }
});



const LoginForm = props => {
    const {classes, logForm: {email, password, rememberPassword},onLogin, updatePath, validError, validOk} = props;

    return (
    <Grid item xs={10}>
        <TextField className={classes.textField}
                   fullWidth
                   label="Email"
                   type="email"
                   value={email.value}
                   onChange={(event, value) => updatePath('logForm.email', event.target.value)}
                   onBlur={() => validators.validateEmail('logForm.email', email.value).then(validOk).catch(validError)}
                   error={email.error}
                   helperText={email.error}

        /><br/>
        <TextField className={classes.textField}
                   fullWidth
                   label="Password"
                   type="password"
                   value={password.value}
                   onChange={(event, value) => updatePath('logForm.password', event.target.value)}
                   onBlur={() => validators.validateMessage('logForm.password', (password.value)?false:"Password must not be empty").then(validOk).catch(validError)}
                   error={password.error}
                   helperText={password.error}
        /><br/>
        <Grid container className={classes.checkBoxWrapper} justify="space-between" alignItems="center">
            <Grid item xs={12} sm={6}>
                <FormControlLabel
                    control={
                        <Checkbox  className={classes.checkBox} checked={rememberPassword.value} onChange={(event, value) => updatePath('logForm.rememberPassword', event.target.checked)} color="primary"/>}
                    label="Remember me"
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <Button className={classes.buttonForgot} disabled size="small" color="primary" >Forgot password</Button>
            </Grid>
        </Grid>
        <Button size="large"
                className={classes.buttonSubmit}
                variant="raised"
                color="primary"
                onClick={onLogin}
                disabled={!email.validated||!password.validated||email.error||password.error}
        >Login</Button>
    </Grid>)
};

const RegisterForm = props => {
    const {classes, regForm: {email, password, passwordConf}, onRegister, updatePath, validError, validOk} = props;
    return (
        <Grid item xs={10}>
            <TextField className={classes.textField}
                       fullWidth  label="Email"
                       type="email"
                       value={email.value}
                       onChange={(event, value) => updatePath('regForm.email', event.target.value)}
                       onBlur={() => validators.validateEmail('regForm.email', email.value).then(validOk).catch(validError)}
                       error={email.error}
                       helperText={email.error}
            /><br/>
            <TextField className={classes.textField}
                       fullWidth label="Password"
                       type="password"
                       value={password.value}
                       onChange={(event, value) => updatePath('regForm.password', event.target.value)}
                       onBlur={() => validators.validatePassword('regForm.password', password.value)
                           .then(([path, value]) => validators.validateMessage([path, 'regForm.passwordConf'], ((value!==passwordConf.value)&&(passwordConf.value))?"Passwords don't match":false))
                           .then(validOk).catch(validError)}
                       error={password.error}
                       helperText={password.error}
            /><br/>
            <TextField className={classes.textField}
                       fullWidth
                       label="Confirm Password"
                       type="password"
                       value={passwordConf.value}
                       onChange={(event, value) => updatePath('regForm.passwordConf', event.target.value)}
                       onBlur={() => validators.validatePassword('regForm.passwordConf',passwordConf.value)
                           .then(([path, value]) => validators.validateMessage([path, 'regForm.password'], ((value!==password.value)&&(password.value))?"Passwords don't match":false))
                           .then(validOk).catch(validError)}
                       error={passwordConf.error}
                       helperText={passwordConf.error}

            /><br/>
            <Button size="large"
                    className={classes.buttonSubmit}
                    variant="raised"
                    color="primary"
                    onClick={onRegister}
                    disabled={!email.validated||!password.validated||!passwordConf.validated||email.error||password.error||passwordConf.error}
            >Register</Button>
        </Grid>)
};


const LoginPaper = props => {
    const {classes, isLogin, updatePath, logForm, regForm, onLogin, onRegister, validError, validOk, fieldReset} = props;


    const form = (isLogin.value===0)?(<LoginForm logForm={logForm}
                                       updatePath={updatePath}
                                       onLogin={onLogin}
                                       classes={classes}
                                       validError={validError}
                                       validOk={validOk}/>):
                            (<RegisterForm regForm={regForm}
                                           updatePath={updatePath}
                                           onRegister={onRegister}
                                           classes={classes}
                                           validOk={validOk}
                                           validError={validError} />);

    const panel = (isLogin.value < 2)?(
            <Grid container className={classes.root} >
                <Grid item xs={12} >
                    <Grid className={classes.screen} container justify="center" alignItems="center" >
                        <Grid item xs={12} sm={6} md={4} lg={2} xl={2}>
                            <Paper className={classes.paper} elevation={4}>
                                <AppBar position="static" color="default">
                                    <Tabs value={isLogin.value} onChange={tabHelper} indicatorColor="primary" textColor="primary" fullWidth>
                                        <Tab label="Login" />
                                        <Tab label="Register" />
                                    </Tabs>
                                </AppBar>
                                <Grid container style={{margin: 'auto'}}  justify="center">
                                    {form}
                                </Grid>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        ):(
            <LoadingScreen/>
    );

    function tabHelper(event, value) {
        updatePath('isLogin', value)
        if (value===0) fieldReset([['regForm.email', 'regForm.password', 'regForm.passwordConf']]) //If login tab, empty register tab
        fieldReset([['logForm.email', 'logForm.password']]) //if register, empty login tab
    }

    return (
        <div>
            {panel}
        </div>
    )
};

LoginPaper.propTypes = {
    isLogin: PropTypes.object.isRequired,
    updatePath: PropTypes.func.isRequired,
    logForm: PropTypes.object.isRequired,
    regForm: PropTypes.object.isRequired,
    onLogin: PropTypes.func.isRequired,
    onRegister: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    validError: PropTypes.func.isRequired
};

export default withStyles(styles)(LoginPaper)
import React from 'react'
import { CircularProgress } from 'material-ui/Progress';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
});

class ProgressCycle extends React.Component {
    state = {
        completed: 0,
    };

    componentDidMount() {
        this.timer = setInterval(this.progress, 20);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    progress = () => {
        const { completed } = this.state;
        this.setState({ completed: completed === 100 ? 0 : completed + 1 });
    };

    render() {
        const { classes } = this.props;
        return (
            <CircularProgress
                className={classes.progress}
                color="primary"
                variant="determinate"
                size={50}
                value={this.state.completed}
            />
        );
    }
}


export default withStyles(styles)(ProgressCycle);
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import Create from 'material-ui-icons/Create';
import ExitToApp from 'material-ui-icons/ExitToApp';
import Tabs, { Tab } from 'material-ui/Tabs';

const styles = {
    root: {
        flexGrow: 1,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

function Layout(props) {
    const { classes } = props;
    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                    <Tabs value={1} fullWidth className={classes.flex}>
                        <Tab label="Test1" />
                        <Tab label="Test2" />
                        <Tab label="Test3" />
                    </Tabs>
                    <IconButton  color="inherit" >
                        <ExitToApp/>
                    </IconButton>
                </Toolbar>
            </AppBar>
        </div>
    );
}

Layout.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Layout);
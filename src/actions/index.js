import $ from 'jquery'
import auth from '../backendapi/auth'
//import {push} from 'react-router-redux'

/**
 *
 * @param path
 * @param value
 */
const updatePath = (path, value) => {
    return {
        type: 'UPDATE',
        payload: {
            path,
            value
        }
    }
};

const validError = ([path, message]) => {
    return {
        type: 'VALIDATION_ERROR',
        payload: {
            path,
            message
        }
    }
};

const validOk = ([path,_]) => {
    return {
        type: 'VALIDATION_OK',
        payload: {
            path
        }
    }
};

const fieldReset = ([path,_]) => {
    return {
        type: 'RESET',
        payload: {
            path
        }
    }
};

const loginAction = (login, password, history) => dispatch => {
    dispatch(updatePath('isLogin', 2))
    auth.login(login, password)
        .then(key => dispatch({type: 'LOGIN_OK', payload: {key}}))
        .then(() => dispatch(updatePath('isLogin', 0)))
        .then(()=>history.push('/landing'))
        .catch(error => dispatch( {type: 'LOGIN_ERROR', payload: {error}}))
};

const registerAction = (login, password) => dispatch => {
    dispatch(updatePath('isLogin', 2))
    auth.register(login, password)
        .then(msg => dispatch({type: 'REGISTER_OK', payload: {msg}}))
        .then(() => dispatch(updatePath('isLogin', 0)))
        .catch(error => dispatch( {type: 'REGISTER_ERROR', payload: {error}}))
};

const apiCall = (login, password) => dispatch => {
    $.ajax({
        url: 'http://google.com',
        success: () => console.log("success"),
        error: () => {dispatch(loginAction(login, password))}
    });
}

export default {updatePath, registerAction, loginAction, apiCall, validError, validOk, fieldReset};

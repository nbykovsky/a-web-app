
const delay = 5000

let userRepository = {
    'test@test.re': {
        password: 'test',
    }
}

const login = (email, password) => {
    return new Promise ((resolve, reject) => {
            if (userRepository[email]&&userRepository[email].password === password) {
                const key = Math.floor((Math.random() * 100) + 1);
                userRepository[email].key = key
                setTimeout(resolve, delay, key)
            } else setTimeout(reject("Email or password is wrong"), delay)
        });
};

const register = (email, password) => {
    return new Promise((resolve, reject) => {
        if (email in userRepository) setInterval(reject("User exists"), delay)
        else {
            userRepository[email] = {}
            userRepository[email].password = password
            setTimeout(resolve, delay, "User registered")
        }
    });
}

export default {login, register}
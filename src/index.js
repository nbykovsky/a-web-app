import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from 'material-ui/CssBaseline';
import { applyMiddleware, compose, createStore } from 'redux';
import LoginPage from './containers/LoginPage';
import LandingPage from './containers/LandingPage'
import rootReducer from './reducers';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom'
import { routerMiddleware } from 'react-router-redux'
import {createBrowserHistory} from 'history';
import { connect } from 'react-redux'

const initState = {
    auth: {
        loggedIn: false,
        key: "",
        error: ""
    },
    logRegForm: {
        isLogin: {
            value:0
        },
        logForm: {
            email: {
                value: "",
                error: "",
                validated: false
            },
            password: {
                value: "",
                error: "",
                validated: false
            },
            rememberPassword: {
                value: false
            }
        },
        regForm: {
            email: {
                value: "",
                error: "",
                validated: false
            },
            password: {
                value: "",
                error: "",
                validated: false
            },
            passwordConf: {
                value: "",
                error: "",
                validated: false
            }
        }
    }
};

const browserHistory = createBrowserHistory();
const routerMDW = routerMiddleware(browserHistory)

const allStoreEnhancers = compose(
    applyMiddleware(thunk),
    applyMiddleware(routerMDW),
    window.devToolsExtension && window.devToolsExtension()
);

const store = createStore(rootReducer, initState, allStoreEnhancers);

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

const PrivateRoute = connect(mapStateToProps)(({component: Component, auth:{loggedIn}, ...rest}) => {
    console.log('loggedIn', loggedIn)
    return (<Route {...rest} render={(props) => {
        if(loggedIn) {
            return (<Component {...props} />)
        } else {
            return (<Redirect to='/login'/>)
        }
    }}/>)
});

function App(){
    return (
        <React.Fragment>
            <CssBaseline />
            <Switch>
                <Route exact path='/login' component={LoginPage}/>
                <PrivateRoute exact path='/landing' component={LandingPage}/>
            </Switch>
        </React.Fragment>
    );

}


ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));

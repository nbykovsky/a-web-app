import React from 'react'
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux'
import {withRouter} from 'react-router'
import Layout from '../components/common/Layout'

const LandingPage = props => {

    return (<div>
        <Layout/>
    </div>)
}

const mapStateToProps = state => {
    return {

    }
}

const mapActionsToProps = (dispatch, props) => {
    return bindActionCreators({
    }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapActionsToProps) (LandingPage));
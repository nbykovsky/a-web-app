import React from 'react';
import LogRegForm from '../components/LoginRegisterForm';
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux';
import actions from '../actions';
import {withRouter} from 'react-router'


class LoginPage extends React.Component {

    render(){
        console.log('router', this.props)
        const {logRegForm: {isLogin, logForm, regForm}, updatePath, validError, validOk, fieldReset, loginAction, registerAction, history} = this.props;
        return (
            <LogRegForm
                isLogin={isLogin}
                updatePath={updatePath}
                logForm={logForm}
                regForm={regForm}
                onLogin={() => {loginAction(logForm.email.value, logForm.password.value, history)}}
                onRegister={() => {registerAction(regForm.email.value, regForm.password.value)}}
                validError={validError}
                validOk={validOk}
                fieldReset={fieldReset}
            />
        );
    }
}

const mapStateToProps = state => {
    return {
        logRegForm: state.logRegForm,
        auth: state.auth
    }
}

const mapActionsToProps = (dispatch, props) => {
    return bindActionCreators({
        updatePath: actions.updatePath,
        loginAction: actions.loginAction,
        registerAction: actions.registerAction,
        validError: actions.validError,
        validOk: actions.validOk,
        fieldReset: actions.fieldReset
    }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapActionsToProps) (LoginPage));
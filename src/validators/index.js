
const validateEmail = (path, email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return new Promise((resolve, reject) => {
        const message = "Email is not valid"
        return re.test(String(email).toLowerCase())?resolve([path, email]):reject([path, message])
    });
}

const validatePassword = (path, password) => {
    return new Promise((resolve, reject) => {
        const message = "Password is too short"
        return (password.length > 2)?resolve([path, password]):reject([path, message])
    })
}

const validateMessage = (path, message) => {
    return new Promise((resolve, reject) => {
        return (!message)?resolve([path, message]):reject([path, message])
    })
}

export default {validateEmail, validatePassword, validateMessage}
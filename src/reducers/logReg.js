import set from 'lodash.set'

export default (state = {}, {type, payload}) => {
    const stateCopy = Object.assign({}, state)
    switch(type) {
        case 'UPDATE': {
            const {path, value} = payload
            if(typeof(path)==="string") set(stateCopy, `${path}.value`, value)
            else path.forEach(p => set(stateCopy, `${p}.value`, value))
            break;
        }
        case 'VALIDATION_ERROR': {
            const {path, message} = payload
            if(typeof(path)==="string")  {
                set(stateCopy, `${path}.error`, message)
                set(stateCopy, `${path}.validated`, true)
            }
            else path.forEach(p => {
                set(stateCopy, `${p}.error`, message)
                set(stateCopy, `${p}.validated`, true)
            })
            break;
        }
        case 'VALIDATION_OK': {
            const {path} = payload
            if(typeof(path)==="string") {
                set(stateCopy, `${path}.error`, "")
                set(stateCopy, `${path}.validated`, true)
            }
            else {
                path.forEach(p => {
                    set(stateCopy, `${p}.error`, "")
                    set(stateCopy, `${p}.validated`, true)
                })
            }
            break;
        }
        case 'RESET': {
            const {path} = payload
            if(typeof(path)==="string") {
                set(stateCopy, `${path}.error`, "")
                set(stateCopy, `${path}.validated`, false)
                set(stateCopy, `${path}.value`, "")
            }
            else {
                path.forEach(p => {
                    set(stateCopy, `${p}.error`, "")
                    set(stateCopy, `${p}.validated`, false)
                    set(stateCopy, `${p}.value`, "")
                })
            }
            break;
        }
    }
    return stateCopy
}
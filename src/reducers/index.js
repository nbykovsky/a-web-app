import { combineReducers } from 'redux';
import LogRegFormReducer from './logReg';
import AuthReducer from './auth';

export default combineReducers({
    logRegForm: LogRegFormReducer,
    auth: AuthReducer
})
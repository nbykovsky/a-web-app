
export default (state = {}, {type, payload}) => {
    const stateCopy = Object.assign({}, state)
    switch(type) {
        case 'LOGIN_OK': {
            const {key} = payload;
            stateCopy.key = key
            stateCopy.loggedIn = true
            console.log(payload, stateCopy)
            break;
        }
        case 'LOGIN_ERROR': {
            const {error} = payload;
            stateCopy.error = error
            break;
        }
        case 'REGISTER_OK': {
            stateCopy.loggedIn = true
            break;
        }
        case 'REGISTER_ERROR': {
            const {error} = payload;
            stateCopy.error = error
            break;
        }

    }
    return stateCopy
}